python-transliterate (1.10.2-6) unstable; urgency=medium

  * Team upload.
  * Make failing tests fail the build
  * Patch-out undeclared usage of "six"

 -- Alexandre Detiste <tchet@debian.org>  Sun, 12 Jan 2025 16:30:17 +0100

python-transliterate (1.10.2-5) unstable; urgency=high

  * Team upload.
  * debian/tests/control: Add missing test-dependency python3-six.
    (Closes: #1022782)

 -- Boyuan Yang <byang@debian.org>  Tue, 25 Oct 2022 14:38:43 -0400

python-transliterate (1.10.2-4) unstable; urgency=medium

  * Fix debian/watch to check GitHub tags instead of releases.
  * Update copyright year.
  * Update Standards-Version.

 -- Edward Betts <edward@4angle.com>  Tue, 07 Jun 2022 17:23:20 +0100

python-transliterate (1.10.2-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Edward Betts ]
  * Use dh-sequence-python3 and dh-sequence-sphinxdoc.
  * Update Standards-Version.
  * Update debhelper-compat to 13.
  * Update debian/watch to version 4.
  * Update copyright year.
  * Set Rules-Requires-Root to no.

 -- Edward Betts <edward@4angle.com>  Sun, 03 Jan 2021 08:38:43 +0000

python-transliterate (1.10.2-2) unstable; urgency=medium

  * debian/rules: set LC_ALL=C.UTF-8 to avoid differences when generating
    egg-info/PKG-INFO with python 3.6 and 3.7
  * debian/control: add Multi-Arch: foreign to python-transliterate-doc

 -- Edward Betts <edward@4angle.com>  Thu, 08 Nov 2018 13:57:39 +0000

python-transliterate (1.10.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs

  [ Edward Betts ]
  * New upstream release.
  * debian/rules: Fix FTBFS by adjusting pybuild call
  * debian/control: Update Standards-Version

 -- Edward Betts <edward@4angle.com>  Mon, 29 Oct 2018 20:00:01 +0000

python-transliterate (1.10.1-1) unstable; urgency=low

  * Initial release. (Closes: #898692)

 -- Edward Betts <edward@4angle.com>  Tue, 15 May 2018 10:00:00 +0100
